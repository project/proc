<?php

namespace Drupal\proc\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultAllowed;
use Drupal\Core\Access\AccessResultNeutral;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\proc\Entity\Proc;
use Drupal\proc\Service\AccessResultService;
use Drupal\proc\Traits\ProcCsvTrait;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Determines access for encryption.
 */
class ProcDecAccessCheck implements AccessInterface {

  use ProcCsvTrait;

  /**
   * The access result service.
   *
   * @var \Drupal\proc\Service\AccessResultService
   */
  protected AccessResultService $accessResultService;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * The current path.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected CurrentPathStack $currentPath;

  /**
   * The logger channel.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new ProcEncAccessCheck.
   *
   * @param \Drupal\proc\Service\AccessResultService $access_result
   *   The access result service.
   * @param \Drupal\Core\Session\AccountProxy $current_user
   *   The current user.
   * @param \Drupal\Core\Path\CurrentPathStack $current_path
   *   The current path.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger channel.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    AccessResultService $access_result,
    AccountProxy $current_user,
    CurrentPathStack $current_path,
    LoggerInterface $logger,
    EntityTypeManagerInterface $entity_type_manager,
  ) {
    $this->accessResultService = $access_result;
    $this->currentUser = $current_user;
    $this->currentPath = $current_path;
    $this->logger = $logger;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Instantiates a new instance of the implementing class using autowiring.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The service container this instance should use.
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('proc.access_result_service'),
      $container->get('current_user'),
      $container->get('path.current'),
      $container->get('logger.factory')->get('proc'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * Checks access.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   *
   * @return \Drupal\Core\Access\AccessResult|\Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultNeutral
   *   The access result.
   */
  public function access(AccountInterface $account): AccessResult|AccessResultAllowed|AccessResultNeutral {
    // Remove IDs from patterns such as 'proc/1,2,3' or 'proc/1,2,3/foo'.
    $proc_ids = $this->getCsvArgument(
      preg_replace(
        '/\/.+/',
        '',
        str_replace('/proc/', '', $this->currentPath->getPath()))
    );
    if (!$proc_ids) {
      $this->logger->notice('No proc IDs found in path: @path', ['@path' => $this->currentPath->getPath()]);
      return $this->accessResultService->forbidden();
    }
    $procs = $this->entityTypeManager->getStorage('proc')->loadMultiple($proc_ids);
    if (count($procs) != count($proc_ids)) {
      $proc_ids_csv = implode(',', $proc_ids);
      $this->logger->notice('Not all proc entities found for the given IDs: @proc_ids', ['@proc_ids' => $proc_ids_csv]);
      return $this->accessResultService->forbidden();
    }
    if ($this->currentUser->hasPermission('administer proc entity')) {
      $this->logger->notice('Allowed access because user has permission to administer proc entity.');
      return $this->accessResultService->allowed();
    };
    foreach ($procs as $proc) {
      if ($proc->getType() != 'cipher') {
        // Log the reason:
        $this->logger->notice('Proc entity @proc_id is not of type cipher.', ['@proc_id' => $proc->id()]);
        return $this->accessResultService->forbidden();
      }
      // Do not allow access unless the user is a recipient.
      if (!$this->isUserRecipient($proc, $this->currentUser->id())) {
        // Log the reason:
        $this->logger->notice('User @user_id is not a recipient of proc entity @proc_id.', [
          '@user_id' => $this->currentUser->id(),
          '@proc_id' => $proc->id(),
        ]);
        return $this->accessResultService->forbidden();
      }
    }

    return $this->accessResultService->allowedIf(
      $account->isAuthenticated() or
      $account->hasPermission('view proc entity')
    )->cachePerUser()->addCacheContexts(['url']);
  }

  /**
   * Checks if a given user is a recipient of a given proc.
   *
   * @param \Drupal\proc\Entity\Proc $proc
   *   The proc entity.
   * @param int $current_user_id
   *   The current user ID.
   *
   * @return bool
   *   True if the user is a recipient, false otherwise.
   */
  private function isUserRecipient(Proc $proc, int $current_user_id): bool {
    $recipients = $proc->get('field_recipients_set')->getValue();
    foreach ($recipients as $recipient) {
      if ($recipient['target_id'] == $current_user_id) {
        return TRUE;
      }
    }
    return FALSE;
  }

}
