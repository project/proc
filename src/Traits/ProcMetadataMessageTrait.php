<?php

namespace Drupal\proc\Traits;

use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Provides a trait for retrieving metadata message.
 */
trait ProcMetadataMessageTrait {

  /**
   * Returns the metadata message.
   *
   * @param mixed $procEntity
   *   The proc entity.
   *
   * @return string
   *   The metadata message.
   */
  public function getMetadataMessage(mixed $procEntity): string {
    if (!$procEntity->getMeta()) {
      return '';
    }
    $encryptionDateTime = DrupalDateTime::createFromTimestamp((int) $procEntity->getMeta()[0]['generation_timestamp'])->format('d/m/Y - H:i:s T');
    return $this->t('<div>Encrypted by %author on %datetime</div>', [
      '%author' => $procEntity->getOwner()->label(),
      '%datetime' => $encryptionDateTime,
    ]);
  }

}
