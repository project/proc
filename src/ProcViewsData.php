<?php

namespace Drupal\proc;

use Drupal\views\EntityViewsData;

/**
 * {@inheritdoc}
 */
class ProcViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData(): array { // phpcs:ignore
    return parent::getViewsData();
  }

}
