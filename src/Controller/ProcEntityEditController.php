<?php

namespace Drupal\proc\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class ProcEntityEditController extends ControllerBase {

  /**
   * Handles the entity edit request.
   */
  public function editEntity(Request $request) {
    $data = json_decode($request->getContent(), TRUE);
    $entity_id = $data['entity_id'];
    $wished_recipients = explode(',', $data['field_wished_recipients_set']);

    // Log the content of $data for debugging:
    \Drupal::logger('proc')->notice('Data: ' . print_r($wished_recipients, TRUE));



//    $new_values = explode($new_values);

    // Load the entity.
    $entity = \Drupal::entityTypeManager()->getStorage('proc')->load($entity_id);
    if ($entity and !empty($wished_recipients)) {
      $entity->set('field_wished_recipients_set', $wished_recipients);
      $entity->save();
      return new JsonResponse(['status' => 'success', 'message' => 'Entity updated successfully.']);
    }
    return new JsonResponse(['status' => 'error', 'message' => 'Entity not found.'], 404);
  }

}
