<?php

namespace Drupal\proc\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\proc\Traits\ProcCsvTrait;
use Drupal\proc\Traits\ProcRecipientTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controller.
 *
 * @package Drupal\proc\Controller
 */
class JsonApiProcController extends ControllerBase {

  use ProcRecipientTrait;
  use ProcCsvTrait;

  /**
   * The current path.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected CurrentPathStack $currentPath;

  /**
   * The Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * JsonApiProcController constructor.
   *
   * @param \Drupal\Core\Path\CurrentPathStack $current_path
   *   The current path.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(CurrentPathStack $current_path, EntityTypeManagerInterface $entityTypeManager) {
    $this->currentPath = $current_path;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Drupal service container.
   *
   * @return static
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('path.current'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Index of keys.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   \Symfony\Component\HttpFoundation\JsonResponse
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function index(): JsonResponse {
    return new JsonResponse(['pubkey' => $this->getData()]);
  }

  /**
   * Get data on latest public keys created.
   *
   * @return array
   *   Array of data containing keys and changed timestamp.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getData(): array {
    $current_path = $this->currentPath->getPath();
    $path_array = explode('/', $current_path);
    $ids_string = $path_array[4];
    $ids = explode(',', $ids_string);

    $search_by = $path_array[5];

    $proc_ids = [];
    foreach ($ids as $id) {
      if (is_numeric($id)) {
        $proc_ids[] = $this->latestKeyringId($id, $search_by);
      }
    }

    $result = [];

    foreach ($proc_ids as $proc_id) {
      if ($proc_id) {
        $proc = $this->entityTypeManager->getStorage('proc')->load($proc_id);
        $result[] = [
          'key' => $proc->get('armored')->getValue()[0]['pubkey'],
          'changed' => $proc->get('created')->getValue()[0]['value'],
        ];
      }
    }

    return $result;
  }

  /**
   * Cipher.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   \Symfony\Component\HttpFoundation\JsonResponse
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function cipher(): JsonResponse {
    return new JsonResponse(['pubkey' => $this->getCihper()]);
  }

  /**
   * Get cipher.
   *
   * @return array
   *   Cipher text and metadata.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getCihper(): array {
    $current_path = $this->currentPath->getPath();
    $path_array = explode('/', $current_path);

    $cipher_ids_string = $path_array[4];
    $cipher_data = [];

    if (empty($cipher_ids_string)) {
      return $cipher_data;
    }

    $proc_ids = $this->getCsvArgument($cipher_ids_string);

    foreach ($proc_ids as $proc_index => $proc_id) {
      $proc = $this->entityTypeManager->getStorage('proc')->load($proc_id);
      if (!$proc) {
        continue;
      }

      $cipher_data[$proc_index] = self::getCipherData($proc, self::getArmored($proc), $proc_id);

    }
    return $cipher_data;
  }

  /**
   * Get cipher data.
   *
   * @param object $proc
   *   The proc entity.
   * @param string $armored
   *   The armored text.
   * @param int $proc_id
   *   The proc ID.
   *
   * @return array
   *   Array of cipher data.
   */
  public function getCipherData(object $proc, string $armored, int $proc_id) {
    $data = [
      'armored' => $armored,
      'source_file_name' => $proc->get('meta')->getValue()[0]['source_file_name'],
      'source_file_size' => $proc->get('meta')->getValue()[0]['source_file_size'],
      'cipher_cid' => $proc_id,
      'proc_owner_uid' => $proc->get('user_id')->getValue()[0]['target_id'],
      'proc_recipients' => $proc->get('field_recipients_set')->getValue(),
      'changed' => $proc->get('changed')->getValue()[0]['value'],
    ];
    if (isset($proc->get('meta')->getValue()[0]['source_input_mode'])) {
      $data['source_input_mode'] = $proc->get('meta')->getValue()[0]['source_input_mode'];
    }
    return $data;
  }

  /**
   * Get armored text.
   *
   * @param object $proc
   *   The proc entity.
   *
   * @return string
   *   The armored text.
   */
  public function getArmored(object $proc): string {
    $armored = '';
    if (
      // If cipher_fid is key in armored field, proc is using stream.
      isset($proc->get('armored')->getValue()[0]['cipher_fid']) &&
      // If cipher_fid is not an array, there is one single file:
      !is_array($proc->get('armored')->getValue()[0]['cipher_fid'])
    ) {
      $storage = $this->entityTypeManager->getStorage('file');
      $file = $storage->load($proc->get('armored')
        ->getValue()[0]['cipher_fid']);
      $armored = file_get_contents($file->getFileUri());
    }
    // If 'cipher' is key at armored field:
    if (isset($proc->get('armored')->getValue()[0]['cipher'])) {
      // Database storage:
      $armored = $proc->get('armored')->getValue()[0]['cipher'];
    }
    // If cipher_fid key is an array, there are multiple files for the
    // storage of the cipher:
    if (
      isset($proc->get('armored')->getValue()[0]['cipher_fid']) &&
      is_array($proc->get('armored')->getValue()[0]['cipher_fid'])
    ) {
      // Concatenate the pieces of the cipher in a single variable:
      foreach ($proc->get('armored')->getValue()[0]['cipher_fid'] as $fid) {
        $armored = $armored . file_get_contents($this->entityTypeManager->getStorage('file')->load($fid)->getFileUri());
      }
    }

    return $armored;

  }

}
