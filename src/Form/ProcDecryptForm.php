<?php

namespace Drupal\proc\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\file\FileRepository;
use Drupal\file\FileUsage\DatabaseFileUsageBackend;
use Drupal\proc\ProcInterface;
use Drupal\proc\ProcKeyManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Decrypt content.
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class ProcDecryptForm extends ProcOpFormBase {

  /**
   * Encryption index.
   *
   * @See \Drupal\proc\ProcInterface::PROC_ENCRYPTION_LIBRARIES
   */
  const OPERATION = 4;


  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The current path.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected CurrentPathStack $currentPath;

  /**
   * The ProcKeyManager service.
   *
   * @var \Drupal\proc\ProcKeyManagerInterface
   */
  protected ProcKeyManagerInterface $procKeyManager;

  /**
   * The entity type manager.
   *
   * @var ?\Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected ?EntityTypeManagerInterface $entityTypeManager;

  /**
   * The logger channel.
   *
   * @var ?\Psr\Log\LoggerInterface
   */
  protected ?LoggerInterface $logger;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * The file repository.
   *
   * @var ?\Drupal\file\Entity\FileRepository
   */
  protected ?FileRepository $fileRepository = NULL;

  /**
   * The file usage service.
   *
   * @var \Drupal\file\FileUsage\DatabaseFileUsageBackend
   */
  protected DatabaseFileUsageBackend $fileUsage;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected Renderer $renderer;

  /**
   * The request stack mock.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * ProcEncryptForm form.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Path\CurrentPathStack $current_path
   *   The current path.
   * @param \Drupal\proc\ProcKeyManagerInterface $procKeyManager
   *   The ProcKeyManager service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   * @param \Drupal\proc\ProcKeyManagerInterface $proc_key_manager
   *   The ProcKeyManager service.
   * @param \Drupal\Core\Session\AccountProxy $current_user
   *   The current user.
   * @param \Drupal\file\Entity\FileRepository $file_repository
   *   The file repository.
   * @param \Drupal\file\FileUsage\DatabaseFileUsageBackend $file_usage
   *   The file usage service.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   *
   * @SuppressWarnings(PHPMD.ExcessiveParameterList)
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    ModuleHandlerInterface $module_handler,
    CurrentPathStack $current_path,
    ProcKeyManagerInterface $procKeyManager,
    EntityTypeManagerInterface $entityTypeManager,
    LoggerInterface $logger,
    ProcKeyManagerInterface $proc_key_manager,
    AccountProxy $current_user,
    FileRepository $file_repository,
    DatabaseFileUsageBackend $file_usage,
    Renderer $renderer,
    RequestStack $request_stack,
  ) {
    parent::__construct(
      $logger,
      $proc_key_manager,
      $entityTypeManager,
      $current_user,
      $file_repository,
      $file_usage,
      $renderer,
      $module_handler,
      $config_factory
    );
    $this->configFactory = $config_factory;
    $this->currentPath = $current_path;
    $this->procKeyManager = $procKeyManager;
    $this->entityTypeManager = $entityTypeManager;
    $this->logger = $logger;
    $this->currentUser = $current_user;
    $this->fileRepository = $file_repository;
    $this->fileUsage = $file_usage;
    $this->renderer = $renderer;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Drupal service container.
   *
   * @return static
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler'),
      $container->get('path.current'),
      $container->get('proc.key_manager'),
      $container->get('entity_type.manager'),
      $container->get('logger.factory')->get('proc'),
      $container->get('proc.key_manager'),
      $container->get('current_user'),
      $container->get('file.repository'),
      $container->get('file.usage'),
      $container->get('renderer'),
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'proc_decrypt_form';
  }

  /**
   * Dynamic title for Decrypt form.
   */
  public function getTitle(): TranslatableMarkup {
    $title = $this->t('Download and decrypt');

    if (!$this->isFileInputMode()) {
      $title = $this->t('Decrypt');
    }

    return $title;
  }

  /**
   * Build the decryption form.
   *
   * @param array $form
   *   Default form array structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object containing current form state.
   *
   * @return array
   *   The render array defining the elements of the form.
   *
   * @throws \Random\RandomException
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $query = $this->requestStack->getCurrentRequest()->query->all();
    $settings = $this->configFactory->get('proc.settings');

    $form = $this->buildDecryptionLink(
      $this->buildPasswordField(
        $settings,
        $query,
        $form
      ),
      $form_state,
      $query,
      'decrypt'
    );

    $ciphers_data = parent::getCipher(
      $this->getCsvArgument(explode('/', $this->currentPath->getPath())[2]),
      FALSE
    );
    if (empty($ciphers_data['pubkey'])) {
      $this->denyAccess();
    }

    $js_procs_settings = [];
    foreach ($ciphers_data['ciphers'] as $cipher_id_data) {
      $js_procs_settings['procs_changed'][] = $cipher_id_data['changed'] ?? 0;
      $js_procs_settings['proc_sources_file_names'][] = $cipher_id_data['source_file_name'] ?? '';
      $js_procs_settings['proc_sources_file_sizes'][] = $cipher_id_data['source_file_size'] ?? 0;
      $js_procs_settings['proc_sources_input_modes'][] = $cipher_id_data['source_input_mode'] ?? 0;
    }

    $form['#attached'] = [
      'library' => $this->getAttachedLibrary(static::OPERATION)['library'],
      'drupalSettings' => [
        'proc' => array_merge(
        [
          'proc_keyring_type' => $ciphers_data['keyring_type'],
          'proc_privkey' => $ciphers_data['privkey'],
          'proc_ids' => array_keys($ciphers_data['ciphers']),
          'proc_pass' => $this->procKeyManager->getPrivKeyMetadata($ciphers_data['privkey'])['proc_pass'],
          // @todo Restore sizes mismatch verification.
          'proc_skip_size_mismatch' => 'TRUE',
        ],
        $js_procs_settings,
        $query,
        ['proc_labels' => _proc_js_labels()]
        ),
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}

  /**
   * Deny access.
   */
  public function denyAccess() {
    throw new AccessDeniedHttpException();
  }

  /**
   * Validate if Proc field input mode sets a file field.
   */
  private function isFileInputMode(): bool {
    $query_string = $this->getRequest()->query->all();
    if (isset($query_string['proc_in_mode'])) {
      $input_mode = $query_string['proc_in_mode'];
    }
    if (!isset($input_mode)) {
      return FALSE;
    }

    return ((int) $input_mode === ProcInterface::INPUT_MODE_FILE);
  }

}
