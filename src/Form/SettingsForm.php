<?php

namespace Drupal\proc\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\proc\ProcInterface;
use Drupal\proc\Service\CssIdentifierCleanerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Protected Content settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The stream_wrapper_manager service.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   */
  protected StreamWrapperManagerInterface $streamWrapperManager;

  /**
   * The CSS identifier cleaner service.
   *
   * @var \Drupal\proc\Service\CssIdentifierCleanerInterface
   */
  private CssIdentifierCleanerInterface $cssIdentifierCleaner;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $streamWrapperManager
   *   The stream wrapper manager service.
   * @param \Drupal\proc\Service\CssIdentifierCleanerInterface $cssIdentifierCleaner
   *   The CSS identifier cleaner service.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typedConfigManager
   *   The typed config manager.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    StreamWrapperManagerInterface $streamWrapperManager,
    CssIdentifierCleanerInterface $cssIdentifierCleaner,
    TypedConfigManagerInterface $typedConfigManager,
  ) {
    parent::__construct($config_factory, $typedConfigManager);
    $this->setConfigFactory($config_factory);
    $this->streamWrapperManager = $streamWrapperManager;
    $this->cssIdentifierCleaner = $cssIdentifierCleaner;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('stream_wrapper_manager'),
      $container->get('proc.css_identifier_cleaner'),
      $container->get('config.typed')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'proc_settings';
  }

  /**
   * {@inheritdoc}
   *
   * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['proc-stream-wrapper'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Global stream wrapper'),
      '#description' => $this->t('Set a stream wrapper for the storage of cipher texts.'),
      '#default_value' => $this->config('proc.settings')
        ->get('proc-stream-wrapper'),
    ];
    $form['proc-enable-stream-wrapper'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable stream wrapper globally'),
      '#description' => $this->t('Enable stream wrapper storage of cipher texts.'),
      '#default_value' => 1,
      '#disabled' => 'disabled',
    ];
    $form['proc-rsa-key-size'] = [
      '#type' => 'select',
      '#title' => $this->t('RSA keys size'),
      '#options' => [
        '2048' => $this->t('2048'),
        '4096' => $this->t('4096'),
      ],
      '#empty_option' => $this->t('-select-'),
      '#description' => $this->t('Set the RSA key size.'),
      '#default_value' => $this->config('proc.settings')
        ->get('proc-rsa-key-size'),
    ];
    $form['proc-file-entity-max-filesize'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Maximum size for encryption in bytes'),
      '#description' => $this->t('Set the maximum size in bytes allowed for encryption. Default if left empty: 50000000 bytes (50 megabytes).'),
      '#default_value' => $this->config('proc.settings')
        ->get('proc-file-entity-max-filesize'),
    ];
    $form['proc-file-block-size'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Block size (lines of armored cipher text)'),
      '#description' => $this->t('Set the size in number of armored cipher text lines for storage blocks. Leave it empty for unlimited. Warning: if there are too few lines and a too big file, there is a risk of time out.'),
      '#default_value' => $this->config('proc.settings')
        ->get('proc-file-block-size'),
    ];
    $form['proc-enable-block-size'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable block size limit'),
      '#description' => $this->t('Enable block size limit. Cipher texts will be split by block. This setting only has effect with stream wrapper storage on.'),
      '#default_value' => $this->config('proc.settings')
        ->get('proc-enable-block-size'),
    ];
    $form['proc-encrypt-button-label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Global standard label for the encrypt link'),
      '#description' => $this->t('The label for attaching an encrypted file. This configuration is overwritten by the widget label on encrypted file fields.'),
      '#default_value' => $this->config('proc.settings')
        ->get('proc-encrypt-button-label'),
    ];
    $form['proc-encrypt-button-label-filled-field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Global label for encryption on a file field when there is a default value.'),
      '#description' => $this->t('The label for creating an encrypted file when the field is filled. Leave it empty for using the standard label.'),
      '#default_value' => $this->config('proc.settings')
        ->get('proc-encrypt-button-label-filled-field'),
    ];
    $form['proc-encrypt-button-class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Classes for the encrypt link'),
      '#description' => $this->t('The CSS class for attaching an encrypted file. Example: button encrypt-button'),
      '#default_value' => $this->config('proc.settings')
        ->get('proc-encrypt-button-class'),
    ];
    $form['proc-allow-password-cache-fields'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable local password cache for fields'),
      '#description' => $this->t('Add an option on decryption for allowing the password to be reused while being cached at client side.'),
      '#default_value' => $this->config('proc.settings')
        ->get('proc-allow-password-cache-fields'),
    ];
    $form['proc-auto-filter-recipients'] = [
      '#type' => 'select',
      '#title' => $this->t('Encryption strategy'),
      '#description' => $this->t("<p><strong>Restrictive</strong> encryption strategy requires the existence of valid PGP key for each and every recipient of a protected content.<br/>
      When using the restrictive encryption strategy, protected content can only be created when all recipients have a valid PGP key.</p>
      <p><strong>Permissive</strong> encryption strategy filters-out from encryption recipients all users without valid PGP key.<br/>
      When using the permissive encryption strategy, protected content is created skipping any user from the list of recipients that still has no valid PGP key.</p>"),
      '#default_value' => $this->config('proc.settings')
        ->get('proc-auto-filter-recipients'),
      '#options' => [
        ProcInterface::STRATEGY_RESTRICTIVE => $this->t('Restrictive'),
        ProcInterface::STRATEGY_PERMISSIVE => $this->t('Permissive'),
      ],
    ];
    $form['proc-suppress-decrypt-link'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Suppress decrypt link from encryption success message'),
      '#description' => $this->t('Decrypt link will be registered in system log but not displayed to users.'),
      '#default_value' => $this->config('proc.settings')
        ->get('proc-suppress-decrypt-link'),
    ];
    $form['proc-suppress-encryption-success-msg'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Suppress entire encryption success message.'),
      '#description' => $this->t('Encryption procedure will be registered in system log but not displayed to users.'),
      '#default_value' => $this->config('proc.settings')
        ->get('proc-suppress-encryption-success-msg'),
    ];
    $form['proc-single-value-remove-button'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add <em>Remove</em> button to single-valued encrypted file fields'),
      '#description' => $this->t('The button will reset the field value.'),
      '#default_value' => $this->config('proc.settings')
        ->get('proc-single-value-remove-button'),
    ];
    $form['proc-single-value-remove-button-label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label for the <em>Remove</em> button'),
      '#description' => $this->t('Leave it empty for using the default label: Remove.'),
      '#default_value' => $this->config('proc.settings')
        ->get('proc-single-value-remove-button-label'),
    ];
    $form['proc-file-enable-autocomplete'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable autocomplete in file fields for non-admin users'),
      '#description' => $this->t('Administrators (or any role with permission <em>administer proc configuration</em>) always have access to autocomplete in order to enhance content management. Consider extending this to non-admin users for a better user experience.'),
      '#default_value' => $this->config('proc.settings')
        ->get('proc-file-enable-autocomplete'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    // phpcs:ignore
    $sw_manager = $this->streamWrapperManager;
    if (!$sw_manager->getScheme($form_state->getValue('proc-stream-wrapper'))) {
      $form_state->setErrorByName('stream_wrapper', $this->t('The provided stream wrapper is invalid.'));
    }
    if (!empty($form_state->getValue('proc-file-entity-max-filesize')) && !is_numeric($form_state->getValue('proc-file-entity-max-filesize'))) {
      $form_state->setErrorByName('proc-file-entity-max-filesize', $this->t('The maximum size for encryption must be a number.'));
    }
    if (!empty($form_state->getValue('proc-file-block-size')) && !is_numeric($form_state->getValue('proc-file-block-size'))) {
      $form_state->setErrorByName('proc-file-block-size', $this->t('The block size must be a number.'));
    }

    $invalid_identifier = $this->validateClassIdentifier($form_state->getValue('proc-encrypt-button-class'));

    if ($invalid_identifier) {
      $form_state->setErrorByName('proc-encrypt-button-class', $this->t('Invalid class identifier.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('proc.settings')
      ->set('proc-stream-wrapper', $form_state->getValue('proc-stream-wrapper'))
      ->set('proc-enable-stream-wrapper', $form_state->getValue('proc-enable-stream-wrapper'))
      ->set('proc-rsa-key-size', $form_state->getValue('proc-rsa-key-size'))
      ->set('proc-file-entity-max-filesize', $form_state->getValue('proc-file-entity-max-filesize'))
      ->set('proc-file-block-size', $form_state->getValue('proc-file-block-size'))
      ->set('proc-enable-block-size', $form_state->getValue('proc-enable-block-size'))
      ->set('proc-encrypt-button-label', $form_state->getValue('proc-encrypt-button-label'))
      ->set('proc-encrypt-button-label-filled-field', $form_state->getValue('proc-encrypt-button-label-filled-field'))
      ->set('proc-encrypt-button-class', $form_state->getValue('proc-encrypt-button-class'))
      ->set('proc-allow-password-cache-fields', $form_state->getValue('proc-allow-password-cache-fields'))
      ->set('proc-auto-filter-recipients', $form_state->getValue('proc-auto-filter-recipients'))
      ->set('proc-suppress-decrypt-link', $form_state->getValue('proc-suppress-decrypt-link'))
      ->set('proc-suppress-encryption-success-msg', $form_state->getValue('proc-suppress-encryption-success-msg'))
      ->set('proc-single-value-remove-button', $form_state->getValue('proc-single-value-remove-button'))
      ->set('proc-single-value-remove-button-label', $form_state->getValue('proc-single-value-remove-button-label'))
      ->set('proc-file-enable-autocomplete', $form_state->getValue('proc-file-enable-autocomplete'))
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['proc.settings'];
  }

  /**
   * Validate the class identifier.
   *
   * @param mixed $classes_string
   *   The string of classes.
   *
   * @return bool
   *   TRUE if the class identifier is invalid.
   */
  private function validateClassIdentifier(mixed $classes_string): bool {
    $classes = explode(' ', $classes_string);
    $invalid_id = FALSE;
    if (!empty($classes_string)) {
      foreach ($classes as $class) {
        if (empty($class)) {
          $invalid_id = TRUE;
          break;
        }
        if ($class != $this->cssIdentifierCleaner->cleanCssIdentifier($class)) {
          $invalid_id = TRUE;
          break;
        }
      }
    }
    return $invalid_id;
  }

}
