<?php

namespace Drupal\Proc\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\file\FileRepository;
use Drupal\file\FileUsage\DatabaseFileUsageBackend;
use Drupal\proc\ProcInterface;
use Drupal\proc\ProcKeyManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Update content.
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class ProcUpdateForm extends ProcOpFormBase {

  /**
   * Update.
   *
   * @See \Drupal\proc\ProcInterface::PROC_ENCRYPTION_LIBRARIES
   */
  const OPERATION = 3;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The current path.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected CurrentPathStack $currentPath;

  /**
   * The ProcKeyManager service.
   *
   * @var \Drupal\proc\ProcKeyManagerInterface
   */
  protected ProcKeyManagerInterface $procKeyManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected ?EntityTypeManagerInterface $entityTypeManager;

  /**
   * The logger channel.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected ?LoggerInterface $logger;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected Renderer $renderer;

  /**
   * ProcEncryptForm form.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Path\CurrentPathStack $current_path
   *   The current path.
   * @param \Drupal\proc\ProcKeyManagerInterface $procKeyManager
   *   The ProcKeyManager service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger channel.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer service.
   * @param \Drupal\Core\Session\AccountProxy $current_user
   *   The current user.
   * @param ?Drupal\file\FileRepository $file_repository
   *   The file repository.
   * @param \Drupal\file\FileUsage\DatabaseFileUsageBackend $file_usage
   *   The file usage service.
   *
   * @SuppressWarnings(PHPMD.ExcessiveParameterList)
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    ModuleHandlerInterface $module_handler,
    CurrentPathStack $current_path,
    ProcKeyManagerInterface $procKeyManager,
    EntityTypeManagerInterface $entityTypeManager,
    LoggerInterface $logger,
    Renderer $renderer,
    AccountProxy $current_user,
    FileRepository $file_repository,
    DatabaseFileUsageBackend $file_usage,
  ) {
    parent::__construct(
      $logger,
      $procKeyManager,
      $entityTypeManager,
      $current_user,
      $file_repository,
      $file_usage,
      $renderer,
      $module_handler,
      $config_factory,
    );

    $this->configFactory = $config_factory;
    $this->moduleHandler = $module_handler;
    $this->currentPath = $current_path;
    $this->procKeyManager = $procKeyManager;
    $this->entityTypeManager = $entityTypeManager;
    $this->logger = $logger;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Drupal service container.
   *
   * @return static
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler'),
      $container->get('path.current'),
      $container->get('proc.key_manager'),
      $container->get('entity_type.manager'),
      $container->get('logger.factory')->get('proc'),
      $container->get('renderer'),
      $container->get('current_user'),
      $container->get('file.repository'),
      $container->get('file.usage'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'proc_update_form';
  }

  /**
   * Build the update form.
   *
   * @param array $form
   *   Default form array structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object containing current form state.
   *
   * @return array
   *   The render array defining the elements of the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $query = $this->getRequest()->query->all();
    $form = $this->buildPasswordField($this->configFactory->get('proc.settings'), $query, $form);
    $form = $this->buildDecryptionLink($form, $form_state, $query, 'update');
    $ciphers_data_result = $this->getCiphersData();

    $js_procs_settings = $ciphers_recipients = $wished_recs = [];
    foreach ($ciphers_data_result['ciphers_data']['ciphers'] as $cipher_id_data) {
      $js_procs_settings['proc_ids'][] = (int) $cipher_id_data['cipher_cid'];
      $js_procs_settings['procs_changed'][] = $cipher_id_data['changed'] ?? 0;
      $js_procs_settings['proc_sources_file_names'][] = $cipher_id_data['source_file_name'] ?? '';
      $js_procs_settings['proc_sources_file_sizes'][] = $cipher_id_data['source_file_size'] ?? 0;
      $js_procs_settings['proc_sources_input_modes'][] = $cipher_id_data['source_input_mode'] ?? 0;
      $ciphers_recipients[] = $cipher_id_data['proc_recipients'];
      $wished_recs[] = $cipher_id_data['proc_wished_recipients'];
    }

    $selected_recipients = [];
    foreach ($ciphers_recipients as $target_proc_key => $target_proc_current) {
      // If the wished recipients are empty, use the current recipients.
      if (empty($wished_recs[$target_proc_key])) {
        $selected_recipients[$js_procs_settings['proc_ids'][$target_proc_key]] = $target_proc_current;
        continue;
      }
      $selected_recipients[$js_procs_settings['proc_ids'][$target_proc_key]] = $wished_recs[$target_proc_key];
    }

    $form_state->set('storage', $selected_recipients);
    $form['#action'] = $this->requestStack->getCurrentRequest()->getBasePath() . '/' . mb_substr($this->currentPath->getPath(), 1);

    $unique_wished = [];
    foreach ($selected_recipients as $proc_recipients) {
      foreach ($proc_recipients as $proc_recipient) {
        $unique_wished[] = $proc_recipient;
      }
    }

    $unique_wished = array_unique($unique_wished);

    $form['#attached'] = [
      'library' => $this->getAttachedLibrary(static::OPERATION)['library'],
      'drupalSettings' => [
        'proc' => array_merge(
        $this->getDrupalSettingsEncryptionUpdate(['created' => $this->procKeyManager->getKeyCreationDates($unique_wished)]),
        $query,
        $js_procs_settings,
        [
          'proc_keyring_type' => $ciphers_data_result['ciphers_data']['keyring_type'],
          'proc_pass' => $this->procKeyManager->getPrivKeyMetadata()['proc_pass'],
          'proc_skip_size_mismatch' => 'TRUE',
          'proc_selected_update_procs_recipients' => json_encode($selected_recipients),
          'proc_selected_recipients' => json_encode($unique_wished),
          'proc_privkey' => $ciphers_data_result['ciphers_data']['privkey'],
          'proc_data' => $this->procKeyManager->getPrivKeyMetadata(),
          'proc_labels' => _proc_js_labels(),
        ],
        ),
      ],
    ];

    $last_cid = 0;
    foreach (array_keys($ciphers_data_result['ciphers_data']['ciphers']) as $cid) {
      // Add hidden fields.
      foreach (ProcInterface::REENCRYPTION_HIDDEN_FIELDS as $hidden_field) {
        $form[$hidden_field . '_' . $cid] = ['#type' => 'hidden'];
      }
      $last_cid = $cid;
    }
    $last_cid_field = $form['cipher_text_' . $last_cid];
    $last_cid_field += ['#attributes' => ['onchange' => 'console.info("Submitting re-encrypted content."); jQuery("#proc-update-form").submit();']];
    $form['cipher_text_' . $last_cid] = $last_cid_field;
    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    // Load proc IDs from form state:
    $storage = $form_state->get('storage');
    $updated_proc_ids = array_keys($storage);
    // Load the proc entities to be updated:
    $procs = $this->entityTypeManager->getStorage('proc')->loadMultiple($updated_proc_ids);

    $updated_fields = [];
    foreach ($procs as $proc_id => $proc) {
      if ($proc->getType() == 'cipher') {
        $recipients = [];
        foreach ($storage[$proc_id] as $recipient_id) {
          $recipients[] = ['target_id' => $recipient_id];
        }
        $updated_fields[$proc_id] = [];
        foreach (ProcInterface::REENCRYPTION_HIDDEN_FIELDS as $hidden_field) {
          $updated_fields[$proc_id][$hidden_field] = $form_state->getValue($hidden_field . '_' . $proc_id);
        }

        $files = $this->saveJsonFiles($updated_fields[$proc_id]['cipher_text']);
        $cipher_fid = $files['file_id'] ?? $files['json_fids'] ?? '';
        $cipher = !empty($cipher_fid) ? ['cipher_fid' => $cipher_fid] : '';

        $prev_recs = $proc->get('field_recipients_set')->getValue();

        $proc->set('armored', $cipher)
          ->set('field_recipients_set', $recipients)
          ->set('field_wished_recipients_set', [])
          ->save();

        if (!is_numeric($proc->id())) {
          $this->messenger()->addMessage($this->t('Unknown error in re-encryption.'));
        }
        if (is_numeric($proc->id())) {
          $prev_rec_ids = [];
          foreach ($prev_recs as $prev_rec) {
            $prev_rec_ids[] = $prev_rec['target_id'];
          }
          $prev_rec_markup = ['#markup' => $this->renderUnorderedList($this->getRecipientEmails($prev_rec_ids))];
          $new_rec_markup = ['#markup' => $this->renderUnorderedList($this->getRecipientEmails($storage[$proc_id]))];
          $success_message = $this->t('Re-encryption is completed for content ID %proc_id (%proc_label).<br>Previous recipient(s): %previous_recipients New recipient(s): %proc_new_recipients', [
            '%proc_id' => $proc_id,
            '%proc_label' => $proc->label(),
            '%previous_recipients' => $this->renderer->renderInIsolation($prev_rec_markup),
            '%proc_new_recipients' => $this->renderer->renderInIsolation($new_rec_markup),
          ]);
          $this->handleFileUsage((int) $files['file_id'], (int) $proc_id, $files['json_fids']);
          $this->getLogger('proc')->info($success_message);
          $this->messenger()->addMessage($success_message);
        }
      }
    }
  }

  /**
   * Title for update form.
   */
  public function getTitle(): TranslatableMarkup {
    return $this->t('Update');
  }

  /**
   * Implements form validation.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    // Validate that all fields are filled in:
    $fields = array_keys($form_state->getValues());
    foreach ($fields as $field) {
      if (str_contains($field, 'cipher_text')) {
        $cipher_text = $form_state->getValue($field);
        if (empty($cipher_text)) {
          $form_state->setErrorByName($field, $this->t('Cipher text is required.'));
        }
        if (!$this->checkPgpOpening($cipher_text)) {
          $form_state->setErrorByName('cipher_text', $this->t('Invalid cipher text format.'));
        }
      }
    }
  }

}
