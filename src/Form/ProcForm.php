<?php

namespace Drupal\proc\Form;

use Drupal\Component\Utility\Bytes;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the proc entity edit forms.
 *
 * @ingroup proc
 */
class ProcForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\proc\Entity\Proc $entity */
    $form = parent::buildForm($form, $form_state);
    $entity = $this->entity;

    if ($form['type']['widget']['#default_value'][0] != 'cipher') {
      unset($form['field_recipients_set']);
      unset($form['field_wished_recipients_set']);
      $form['key_size'] = [
        '#title' => $this->t('Key Size'),
        '#type' => 'textfield',
        '#default_value' => $this->formatSize($entity->get('meta')->getValue()[0]['key_size']),
        '#disabled' => TRUE,
        '#weight' => 1,
      ];
    }
    if (isset($entity->get('meta')->getValue()[0]['source_file_size'])) {
      $form['size'] = [
        '#title' => $this->t('Size'),
        '#type' => 'textfield',
        '#default_value' => $this->formatSize($entity->get('meta')->getValue()[0]['source_file_size']),
        '#disabled' => TRUE,
        '#weight' => 1,
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function save(array $form, FormStateInterface $form_state): void {
    $form_state->setRedirect('entity.proc.collection');
    $entity = $this->getEntity();
    $entity->save();
  }

  /**
   * Formats a size in bytes into a human-readable string.
   *
   * @param int $size
   *   The size in bytes.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   A human-readable size string.
   */
  protected function formatSize($size) {
    if ($size < Bytes::KILOBYTE) {
      return $this->t('@size bytes', ['@size' => $size]);
    }

    $size = $size / Bytes::KILOBYTE;
    return $this->t('@size KB', ['@size' => round($size, 2)]);
  }

}
