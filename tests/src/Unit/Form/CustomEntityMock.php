<?php
// phpcs:ignoreFile

namespace Drupal\Tests\proc\Unit\Form;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Custom entity mock.
 *
 * @SuppressWarnings(PHPMD)
 */
class CustomEntityMock implements EntityInterface {

  /**
   *
   */
  public function id() {}

  /**
   *
   */
  public function uuid() {}

  /**
   *
   */
  public function getEntityTypeId() {}

  /**
   *
   */
  public function bundle() {}

  /**
   *
   */
  public function get($property_name) {}

  /**
   *
   */
  public function set($property_name, $value) {}

  /**
   *
   */
  public function hasField($field_name) {
    return TRUE;
  }

  /**
   *
   */
  public function getFieldDefinition($field_name) {}

  /**
   *
   */
  public function getTranslation($langcode) {}

  /**
   *
   */
  public function getUntranslated() {}

  /**
   *
   */
  public function language() {}

  /**
   *
   */
  public function getTranslationLanguages($include_default = TRUE) {}

  /**
   *
   */
  public function isTranslatable() {}

  /**
   *
   */
  public function isNew() {}

  /**
   *
   */
  public function isDefaultTranslation() {}

  /**
   *
   */
  public function getOriginalId() {}

  /**
   *
   */
  public function getEntityType() {}

  /**
   *
   */
  public function getTypedData() {}

  /**
   *
   */
  public function getCacheTags() {}

  /**
   *
   */
  public function getCacheContexts() {}

  /**
   *
   */
  public function getCacheMaxAge() {}

  /**
   *
   */
  public function toUrl($rel = 'canonical', array $options = []) {}

  /**
   *
   */
  public function toLink($text = NULL, $rel = 'canonical', array $options = []) {}

  /**
   *
   */
  public function toArray() {}

  /**
   *
   */
  public function getIterator() {}

  /**
   *
   */
  public function offsetExists($offset) {}

  /**
   *
   */
  public function offsetGet($offset) {}

  /**
   *
   */
  public function offsetSet($offset, $value) {}

  /**
   *
   */
  public function offsetUnset($offset) {}

  /**
   *
   */
  public function __sleep() {}

  /**
   *
   */
  public function __wakeup() {}

  /**
   *
   */
  public function access($operation, ?AccountInterface $account = NULL, $return_as_object = FALSE) {
  }

  /**
   *
   */
  public function enforceIsNew($value = TRUE) {
  }

  /**
   *
   */
  public function label() {
  }

  /**
   *
   */
  public function hasLinkTemplate($key) {
  }

  /**
   *
   */
  public function uriRelationships() {
  }

  /**
   *
   */
  public static function load($id) {
  }

  /**
   *
   */
  public static function loadMultiple(?array $ids = NULL) {
  }

  /**
   *
   */
  public static function create(array $values = []) {
  }

  /**
   *
   */
  public function save() {
  }

  /**
   *
   */
  public function delete() {
  }

  /**
   *
   */
  public function preSave(EntityStorageInterface $storage) {
  }

  /**
   *
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
  }

  /**
   *
   */
  public static function preCreate(EntityStorageInterface $storage, array &$values) {
  }

  /**
   *
   */
  public function postCreate(EntityStorageInterface $storage) {
  }

  /**
   *
   */
  public static function preDelete(EntityStorageInterface $storage, array $entities) {
  }

  /**
   *
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
  }

  /**
   *
   */
  public static function postLoad(EntityStorageInterface $storage, array &$entities) {
  }

  /**
   *
   */
  public function createDuplicate() {
  }

  /**
   *
   */
  public function referencedEntities() {
  }

  /**
   *
   */
  public function getCacheTagsToInvalidate() {
  }

  /**
   *
   */
  public function setOriginalId($id) {
  }

  /**
   *
   */
  public function getConfigDependencyKey() {
  }

  /**
   *
   */
  public function getConfigDependencyName() {
  }

  /**
   *
   */
  public function getConfigTarget() {
  }

  /**
   *
   */
  public function addCacheContexts(array $cache_contexts) {
  }

  /**
   *
   */
  public function addCacheTags(array $cache_tags) {
  }

  /**
   *
   */
  public function mergeCacheMaxAge($max_age) {
  }

  /**
   *
   */
  public function addCacheableDependency($other_object) {
  }

}
