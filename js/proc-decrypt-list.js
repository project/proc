/**
 * @file
 * Decrypts cipher texts in a list.
 */
import { feedPassCache } from './proc-feed-pass-cache-module.js';
import { getPassSessionCachingSalt } from './proc-caching-salt-module.js';

(function($, Drupal, once) {
  Drupal.behaviors.ProcBehavior = {
    async attach(context, settings) {
      if (context.id !== '') {
        const cipherTexts = [];
        const cipherTextTypes = [];
        document.querySelectorAll('[data-proc-list-item^="proc-list-item"]').forEach((procItem) => {
          cipherTexts.push(procItem.getAttribute('data-proc-list-item-cipher-text'));
          cipherTextTypes.push(procItem.getAttribute('data-proc-list-item-type'));
        });
        const passwordSessionCachingSalt = getPassSessionCachingSalt();
        const cachedPassword = sessionStorage.getItem(
          `proc.password_cache.key_user_id.${drupalSettings.user.uid}`,
        );
        if (!cachedPassword) {
          // There is no cached password.
          const dialogDiv = $(document.createElement('div'));
          dialogDiv.attr('id', 'proc-decrypt-dialog');
          const passwordField = $(document.createElement('input'));
          passwordField.attr('type', 'password');
          passwordField.attr('id', 'password');
          const passwordLabel = $(document.createElement('label'));
          passwordLabel.attr('for', 'password');
          passwordLabel.html('Password');
          passwordField[0].addEventListener('input', function (evt) {
            let pass = this.value;
            decryptListItems(pass).then(function (result) {
              if (result) {
                feedPassCache('1', drupalSettings.proc.proc_keyring_type, pass, passwordSessionCachingSalt, openpgp);
              }
            });
          });
          dialogDiv.append(passwordLabel);
          dialogDiv.append(passwordField);
          dialogDiv.dialog();
        }
        if (cachedPassword) {
          try {
            await (async () => {
              const messageSource = await openpgp.readMessage({
                armoredMessage: sessionStorage.getItem(
                  `proc.password_cache.key_user_id.${drupalSettings.user.uid}`,
                ),
              });
              const { data: secretPassString } = await openpgp.decrypt({
                message: messageSource,
                passwords: [passwordSessionCachingSalt],
              });
              await decryptAllItems(
                await decryptPrivateKey(secretPassString)
              );
            })();
          } catch (err) {
            // This error may happen because of a change in the browser fingerprint.
            console.info(err);
          }
        }
        /**
         * Decrypts the private key.
         *
         * Decrypts the private key given a correct passphrase of symmetric encryption.
         *
         * @param password
         *   Password that was typed in or taken from cached.
         */
        async function decryptPrivateKey(password) {
          const passphrase = drupalSettings.proc.proc_pass.concat(password);
          const privateKey = await openpgp.readPrivateKey({
            armoredKey: drupalSettings.proc.proc_privkey,
          });
          return await openpgp
            .decryptKey({
              privateKey,
              passphrase,
            })
            .catch(function (err) {
              const error_message = new Drupal.Message();
              error_message.clear();
              error_message.add(`${Drupal.t(err)}`, {type: 'error'});
            });
        }
        /**
         * Decrypts all listed items.
         *
         * Decrypts all listed items with the decrypted private key.
         *
         * @param decryptedPrivateKey
         *   Symmetrically decrypted private key.
         */
        async function decryptAllItems(decryptedPrivateKey) {
          for (const cipherText of cipherTexts) {
            if (cipherText) {
              const message = await openpgp.readMessage({
                armoredMessage: cipherText,
              });
              const decrypted = await openpgp
                .decrypt({
                  decryptionKeys: decryptedPrivateKey,
                  message,
                  format: 'binary',
                })
                .catch(function (err) {
                  const error_message = new Drupal.Message();
                  error_message.add(`${Drupal.t(err)}`, {type: 'error'});
                });
              if (decrypted) {
                await renderMedia(cipherTexts.indexOf(cipherText), decrypted);
              }
            }
          }
        }
        /**
         * Decrypts all listed items from uncached password dialog.
         *
         * Decrypts all listed items with a correct passphrase of symmetric encryption.
         *
         * @param password
         *   Passphrase for symmetrical decryption of private key.
         */
        async function decryptListItems(password) {
          const decryptedPrivateKey = await decryptPrivateKey(password);
          if (decryptedPrivateKey) {
            await decryptAllItems(decryptedPrivateKey);
            // Close dialog.
            $('#proc-decrypt-dialog').dialog('close');
            return true;
          }
        }
        /**
         * Renders the media according to mime type.
         *
         * @param cipherTextIndex
         * @param decrypted
         * @returns {Promise<void>}
         */
        async function renderMedia(cipherTextIndex, decrypted) {
          let procElement = document.querySelectorAll('[data-proc-list-item^="proc-list-item"]')[cipherTextIndex];
          let contentType = cipherTextTypes[cipherTextIndex];
          let url = URL.createObjectURL(new Blob([decrypted.data]));
          const setTextContent = (element, text) => {
            if (element.innerText) {
              element.innerText = text;
            } else {
              element.value = text;
            }
          };
          const renderMedia = (tagName) => {
            const mediaElement = document.createElement(tagName.toUpperCase());
            mediaElement.src = url;
            mediaElement.controls = true;
            if (!procElement.innerHTML.includes('blob')) {
              const wrapper = document.createElement('div');
              wrapper.appendChild(mediaElement);
              procElement.appendChild(wrapper);
            }
          };
          if (contentType === 'text/plain') {
            setTextContent(procElement, new TextDecoder().decode(decrypted.data));
          } else if (contentType.includes('image')) {
            if (contentType.includes('svg')) {
              const parser = new DOMParser();
              const svg = parser.parseFromString(
                new TextDecoder().decode(decrypted.data),
                'text/html'
              ).querySelector('svg');
              await procAppendElement(procElement, svg);
            }
            else {
              const image = new Image();
              image.src = url;
              if (!procElement.innerHTML.includes('blob')) {
                await procAppendElement(procElement, image);
              }
            }
          } else if (contentType.includes('video')) {
            renderMedia("video");
          } else if (contentType.includes('audio')) {
            renderMedia("audio");
          } else {
            await linkUnrecognizedMimeTypes(decrypted, procElement);
          }
        }

        /**
         * Links unrecognized mime types.
         *
         * @param decrypted
         * @param procElement
         * @returns {Promise<void>}
         */
        async function linkUnrecognizedMimeTypes(decrypted, procElement) {
          // Add temporary download link the same way as in proc-decrypt.js.
          const temporaryDownloadLink = document.createElement('a');
          temporaryDownloadLink.setAttribute(
            'href',
            URL.createObjectURL(new Blob([decrypted.data], {
              type: 'application/octet-binary',
              endings: 'native',
            }))
          );
          temporaryDownloadLink.setAttribute(
            'download',
            procElement.innerText,
          );
          temporaryDownloadLink.innerText = Drupal.t('Download');
          // If it has not yet been added.
          if (!procElement.innerHTML.includes('blob')) {
            procElement.appendChild(document.createElement('div').appendChild(temporaryDownloadLink));
          }
        }
        /**
         * Appends element to another element.
         *
         * @param procElement
         * @param targetElement
         * @returns {Promise<void>}
         */
        async function procAppendElement(procElement, targetElement) {
          procElement.appendChild(document.createElement('div').appendChild(targetElement));
        }
      }
    },
  };
})(jQuery, Drupal, once);
