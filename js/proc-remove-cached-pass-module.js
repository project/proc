/**
 * @file
 * Remove cached password.
 */

/**
 * Remove cached password.
 */
export function removeCachedPassword() {
  sessionStorage.removeItem(`proc.password_cache.key_user_id.${drupalSettings.user.uid}`);
  sessionStorage.removeItem(`proc.password_cache_type.key_user_id.${drupalSettings.user.uid}`);
}
