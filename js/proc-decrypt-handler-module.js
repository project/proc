/**
 * @file
 * Decrypts the cipher text.
 */

import { feedPassCache } from './proc-feed-pass-cache-module.js';
import { getPassSessionCachingSalt } from './proc-caching-salt-module.js';
import { removeCachedPassword } from './proc-remove-cached-pass-module.js';
import { processDecryption } from './proc-process-decryption-module.js';
import { getProcUrls } from './proc-get-urls-module.js';

/**
 * Decrypt the ciphers.
 *
 * @param $
 * @param {string} opLink
 *
 * @returns {Promise<void>}
 */
export async function decryptHandler($, opLink) {
  const cache_password = document.querySelector(
    '[name="cache_password"]',
  )
    ? document.querySelector('[name="cache_password"]').value
    : 0;
  let passwordSelector = $('input[name=password]');
  const secretPassString = passwordSelector[0].value;
  await feedPassCache(cache_password, drupalSettings.proc.proc_keyring_type, secretPassString, getPassSessionCachingSalt(), openpgp);
  if (cache_password === '0' || drupalSettings.proc.proc_keyring_type === 'keyring') {
    // Remove password from cache.
    removeCachedPassword();
    passwordSelector[0].value = '';
  }
  if (!$('#proc-decrypting-info')[0]) {
    console.info(drupalSettings.proc.proc_labels.proc_introducing_decryption);
  }
  const temporaryDownloadLink = document.createElement('a');
  temporaryDownloadLink.style.display = 'none';
  document.body.appendChild(temporaryDownloadLink);
  await processDecryption(
    $,
    await getProcUrls(drupalSettings),
    openpgp,
    await openpgp.readPrivateKey({
      armoredKey: drupalSettings.proc.proc_privkey
    }),
    drupalSettings.proc.proc_pass.concat(secretPassString),
    temporaryDownloadLink,
    opLink
  );
  document.body.removeChild(temporaryDownloadLink);
}
