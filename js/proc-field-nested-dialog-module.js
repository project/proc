/**
 * @file
 * Nested dialog module.
 */

/**
 * Set the class of the close button of a dialog.
 *
 * @returns {void}
 */
export function setCloseDialogBtnProcFieldClass() {
  // For each close button of a dialog:
  document.querySelectorAll('.ui-dialog-titlebar-close').forEach(function(closeButton) {
    // Check if the parent contains the current field name:
    closeButton.parentElement.parentElement.classList.forEach(function(className) {
      if (className.includes('proc-encrypt-dialog')) {
        closeButton.classList.add('class-' + drupalSettings.proc.proc_field_name);
      }
    });
  });
}
