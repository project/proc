/**
 * Encrypts a message with the given public keys.
 *
 * @returns {promise}
 */
export async function procEncrypt(messageOptions, publicKeys, openpgp) {
  const message = await openpgp.createMessage(messageOptions);
  return await openpgp.encrypt({
    encryptionKeys: publicKeys,
    message,
    format: 'armored',
    config: {
      preferredCompressionAlgorithm: openpgp.enums.compression.zip,
    },
  });
}
