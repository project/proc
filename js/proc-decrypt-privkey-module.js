/**
 * @file
 * Openpgp decryption handler.
 */

/**
 * Decrypt the private key.
 *
 * @param $
 *   jQuery.
 * @param openpgp
 *   Openpgp library.
 * @param privateKey
 *   Encrypted private key.
 * @param passphrase
 *   Passphrase for the private key.
 *
 * @returns {Promise<*>}
 */
export async function decryptPrivateKey($, openpgp, privateKey, passphrase) {
  return await openpgp
    .decryptKey({
      privateKey,
      passphrase,
    })
    .catch(function (err) {
      console.warn(err);
      const message = new Drupal.Message();
      $('form[id^="proc"]').prepend(message.messageWrapper);
      message.add(`${Drupal.t(err)}`, {type: 'error'});
      const decryptionLinkElement = $('a#decryption-link');
      if (decryptionLinkElement[0]) {
        const fileUrl = decryptionLinkElement[0].href;
        URL.revokeObjectURL(fileUrl);
        decryptionLinkElement.removeAttr('href');
      }
    });
}
