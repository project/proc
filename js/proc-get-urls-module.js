/**
 * Get the URLs of the ciphers to be decrypted.
 *
 * @param drupalSettings
 * @returns {Promise<*>}
 */
export async function getProcUrls(drupalSettings) {
  return await drupalSettings.proc.proc_ids.map(
    (cipherId, cipherIdIndex) =>
      `${window.location.origin +
      drupalSettings.path.baseUrl}api/proc/getcipher/${cipherId}/?cipherchanged=${
        drupalSettings.proc.procs_changed[cipherIdIndex]
      }`,
  );
}
