/**
 * @file
 * Initialize password caching.
 */

/**
 * Initialize password caching.
 */
export function initPasswordCaching() {
  console.info('Initializing password caching.');
  // Initialize password caching.
  if (document.querySelector('[name="cache_password"]')) {
    // Caching password is disabled by default.
    document.querySelector('[name="cache_password"]').value = 0;
    if (drupalSettings.proc.proc_cache_password_mode === '2') {
      // Caching password is enabled by default.
      document.querySelector('[name="cache_password"]').value = 1;
    }
  }
}
