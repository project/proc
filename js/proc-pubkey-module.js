/**
 * @file
 * Public key module.
 */

/**
 * Helper function for reading public keys from local storage and remote server
 *
 * @param $
 * @param drupalSettings
 * @param openpgp
 * @returns {Promise<Awaited<armoredKey>[]>}
 */
export async function procReadKey($, drupalSettings, openpgp) {
  const recipientsPubkeys = [];
  const remoteKey = [];
  const recipientsUidsKeysChanged = JSON.parse(
    drupalSettings.proc.proc_recipients_pubkeys_changed,
  );
  for (const userIdIterator in recipientsUidsKeysChanged) {
    const localKey = localStorage.getItem(
      `proc.key_user_id.${userIdIterator}.${recipientsUidsKeysChanged[userIdIterator]}`,
    );
    if (localKey) {
      console.info('Key found in local storage');
      recipientsPubkeys.push(localKey);
    } else {
      const storageKeys = Object.keys(localStorage);
      if (storageKeys.length > 0) {
        storageKeys.forEach(function(storageKey, storageKeyIndex) {
          if (
            storageKey.startsWith(
              `proc.key_user_id.${userIdIterator}`,
            )
          ) {
            localStorage.removeItem(storageKeys[storageKeyIndex]);
          }
        });
      }
      remoteKey.push(userIdIterator);
    }
  }
  if (remoteKey.length > 0) {
    const remoteKeyCsv = remoteKey.join(',');
    const pubKeyAjax = async remoteKeyCsv => {
      const response = await fetch(
        `${window.location.origin +
        drupalSettings.path.baseUrl}api/proc/getpubkey/${remoteKeyCsv}/user_id`,
      );
      const pubkeysJson = await response.json();
      if (pubkeysJson.pubkey.length > 0) {
        pubkeysJson.pubkey.forEach(function(pubkey, index) {
          recipientsPubkeys.push(pubkey.key);
          try {
            localStorage.setItem(
              `proc.key_user_id.${remoteKey[index]}.${pubkey.changed}`,
              pubkey.key,
            );
          } catch (error) {
            console.warn(error);
          }
        });
      }
    };
    await pubKeyAjax(remoteKeyCsv);
  }
  return await Promise.all(
    recipientsPubkeys.map(armoredKey =>
      openpgp.readKey({
        armoredKey,
      }),
    ),
  );
}
