/**
 * Feed password to cache.
 */

import { removeCachedPassword } from './proc-remove-cached-pass-module.js';

/**
 * Feeds the password to the cache.
 *
 * @param cache_password
 *   Cache password.
 * @param keyringCacheType
 *   Keyring cache type.
 * @param secretPassString
 *   Secret pass string.
 * @param passwordSessionCachingSalt
 *   Password session caching salt.
 * @param openpgp
 *   Openpgp library.
 *
 * @returns {Promise<void>}
 */
export async function feedPassCache(cache_password, keyringCacheType, secretPassString, passwordSessionCachingSalt, openpgp) {
  if (cache_password === '1' && keyringCacheType !== 'keyring') {
    // Cache password.
    await (async () => {
      const message = await openpgp.createMessage({
        text: secretPassString,
      });
      // Check if an expiration time is already set.
      if (
        sessionStorage.getItem(
          `proc.password_cache_type.key_user_id.${drupalSettings.user.uid}`,
        )
      ) {
        const remainingTime =
          sessionStorage.getItem(
            `proc.password_cache_type.key_user_id.${drupalSettings.user.uid}`,
          ) - new Date().getTime();
        if (remainingTime <= 0) {
          // Remove cached password.
          removeCachedPassword();
        }
      } else {
        sessionStorage.setItem(
          `proc.password_cache.key_user_id.${drupalSettings.user.uid}`,
          await openpgp.encrypt({
            message,
            passwords: [passwordSessionCachingSalt],
          }),
        );
        // If expiration time is set to 1 hour, then count 1 hour from now.
        let expirationTime = new Date().getTime() + 3600000;
        // If expiration time is set to 2 hours, then count 2 hours from now.
        if (keyringCacheType === 'keyring_2h') {
          expirationTime += 3600000;
        }
        sessionStorage.setItem(
          `proc.password_cache_type.key_user_id.${drupalSettings.user.uid}`,
          expirationTime,
        );
      }
    })();
  }
}
