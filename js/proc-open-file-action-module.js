/**
 * @file
 * Open the file.
 */

import { processField } from './proc-process-field-module.js';

/**
 * Open the content.
 *
 * @param $
 * @param decrypted
 * @param temporaryDownloadLink
 * @param cipherIndex
 * @param opLink
 * @returns {Promise<void>}
 */
export const openFileAction = async (
  $,
  decrypted,
  temporaryDownloadLink,
  cipherIndex,
  opLink,
) => {
  if (opLink == null) {
    opLink = $('#decryption-link');
  }
  const message = new Drupal.Message();
  const blob = new Blob([decrypted.data], {
    type: 'application/octet-binary',
    endings: 'native',
  });
  temporaryDownloadLink.setAttribute('href', URL.createObjectURL(blob));
  const openActionLabel = drupalSettings.proc.proc_labels.proc_open_file_state;
  if (opLink.text() !== openActionLabel) {
    opLink.text(openActionLabel);
    opLink.removeClass('active');
    message.add(`${drupalSettings.proc.proc_labels.proc_decryption_success}`, {type: 'status'});
  }
  if (
    blob.size.toString() ===
    drupalSettings.proc.proc_sources_file_sizes[cipherIndex] ||
    drupalSettings.proc.proc_skip_size_mismatch === 'TRUE'
  ) {
    temporaryDownloadLink.setAttribute(
      'download',
      drupalSettings.proc.proc_sources_file_names[cipherIndex],
    );
    if (drupalSettings.proc.proc_decryption_mode === '0' || !drupalSettings.proc.proc_decryption_mode) {
      temporaryDownloadLink.click();
    }
    // If decryption was triggered from a field.
    if (drupalSettings.proc.proc_field_name) {
      drupalSettings.proc.proc_field_name = drupalSettings.proc.proc_field_name.replace(
        /'/g,
        '',
      );
      processField($, drupalSettings, temporaryDownloadLink, blob);
    }

  } else {
    message.add(`${drupalSettings.proc.proc_labels.proc_decryption_size_mismatch}`, {type: 'error'});
  }
};
