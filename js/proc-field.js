/**
 * @file
 * Helper for entity reference proc field.
 */
(function($, Drupal, drupalSettings, once) {
  const ProcBehavior = {
    attach(context, settings) {
      once('proc-decrypt', 'html', context).forEach(initProcBehavior);
    },
  };
  /**
   * Initialize proc behavior.
   */
  function initProcBehavior() {
    const { procFieldElements, encryptCheckboxes, procFields } = selectProcElements();
    const procFetchers = document.querySelectorAll('[proc="true"][data-proc-fetcher]');

    setupProcElements(procFieldElements, encryptCheckboxes, procFields);
    handleProcFetchers(procFetchers);
  }
  /**
   * Select proc elements.
   * @returns {{procFieldElements: NodeListOf<Element>, encryptCheckboxes: NodeListOf<Element>, procFields: *[]}}
   */
  function selectProcElements() {
    const procFieldElements = document.querySelectorAll(
      '[proc="true"]:not(.form-autocomplete)'
    );
    const procFields = Array.from(procFieldElements, extractFieldName);
    const encryptCheckboxes = document.querySelectorAll('[id^="encrypt-checkbox-"]');

    return { procFieldElements, encryptCheckboxes, procFields };
  }
  /**
   * Setup proc elements.
   * @param procFieldElements
   * @param encryptCheckboxes
   * @param procFields
   */
  function setupProcElements(procFieldElements, encryptCheckboxes, procFields) {
    setTextSiblingsAttribute(procFields, procFieldElements, encryptCheckboxes);
    setInitialStateCheckboxes(procFieldElements, encryptCheckboxes);
    disableEncryptCheckboxIfEmpty(procFieldElements);
    switchSubmitOnInput(procFields, procFieldElements);
    switchSubmitOnCheckbox(encryptCheckboxes);
  }
  /**
   * Handle proc fetchers.
   * @param procFetchers
   */
  function handleProcFetchers(procFetchers) {
    const endpointCache = new Map();

    procFetchers.forEach(procFetcher => {
      const fetcherRecipients = procFetcher.getAttribute('data-proc-recipients') || '';
      const fetcherEndpoint = procFetcher.getAttribute('data-proc-fetcher');
      const fetcherProcId = procFetcher.getAttribute('data-proc-id') || '';

      if (!procFetcher.hasAttribute('data-proc-wished-recipients') && fetcherRecipients) {
        processFetcher(endpointCache, fetcherEndpoint, fetcherRecipients, fetcherProcId);
      }
    });
  }
  /**
   * Process fetcher.
   * @param endpointCache
   * @param endpoint
   * @param currentRecipients
   * @param procId
   * @returns {Promise<void>}
   */
  async function processFetcher(endpointCache, endpoint, currentRecipients, procId) {
    try {
      const csv = await getCachedEndpointCSV(endpointCache, endpoint);
      if (csv !== currentRecipients) {
        await updateWishedRecipients(procId, csv);
      }
    } catch (error) {
      console.error('Error processing fetcher:', error);
    }
  }
  /**
   * Get cached endpoint CSV.
   * @param endpointCache
   * @param endpoint
   * @returns {*}
   */
  function getCachedEndpointCSV(endpointCache, endpoint) {
    if (!endpointCache.has(endpoint)) {
      endpointCache.set(endpoint, fetchEndpointRecipientsCSV(endpoint));
    }
    return endpointCache.get(endpoint);
  }
  /**
   * Fetch endpoint recipients CSV.
   * @param endpoint
   * @returns {Promise<string>}
   */
  async function fetchEndpointRecipientsCSV(endpoint) {
    try {
      console.info('Fetching recipients from endpoint:', endpoint);
      const response = await fetch(endpoint, { method: 'get' });
      const result = await response.json();
      const items = typeof result === 'object' ? Object.values(result) : result;

      const ids = items.map(item => item.id || item.uid).filter(Boolean);
      return ids.join();
    } catch (error) {
      console.error('Error fetching endpoint:', endpoint, error);
      return '';
    }
  }
  /**
   * Update wished recipients.
   * @param procId
   * @param recipientsCSV
   * @returns {Promise<void>}
   */
  async function updateWishedRecipients(procId, recipientsCSV) {
    console.info(`Updating wished recipients for proc ID ${procId}`);

    try {
      const token = await getCsrfToken();
      const response = await fetch(`${window.location.origin + drupalSettings.path.baseUrl}proc/edit-entity`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'X-CSRF-Token': token,
        },
        body: JSON.stringify({
          entity_id: procId,
          field_wished_recipients_set: recipientsCSV,
        }),
      });

      const result = await response.json();
      if (result.status !== 'success') {
        throw new Error(result.message || 'Unknown error');
      }
      console.log('Entity updated successfully');
    } catch (error) {
      console.error('Error updating entity:', error);
    }
  }
  /**
   * Get CSRF token.
   * @returns {Promise<string>}
   */
  async function getCsrfToken() {
    const response = await fetch(`${window.location.origin + drupalSettings.path.baseUrl}session/token`, {
      method: 'GET',
      credentials: 'same-origin'
    });
    return response.text();
  }
  /**
   * Extract field name from field element.
   * @param fieldElement
   * @return fieldName
   */
  function extractFieldName(fieldElement) {
    return fieldElement.attributes.name.value.split('[')[0];
  }
  /**
   * Set initial state of encrypt checkboxes.
   * @param procFieldElements
   * @param encryptCheckboxes
   */
  function setInitialStateCheckboxes(procFieldElements, encryptCheckboxes) {
    procFieldElements.forEach((fieldElement, index) => {
      encryptCheckboxes[index].disabled = fieldElement.value === '';
    });
  }
  /**
   * Disable encrypt checkbox if and only if field is empty.
   * @param procFieldElements
   */
  function disableEncryptCheckboxIfEmpty(procFieldElements) {
    procFieldElements.forEach((fieldElement, index) => {
      fieldElement.addEventListener('input', inputChangeCheckbox);
    });
  }
  /**
   * Encrypt checkbox change event handler.
   * @param inputEvent
   */
  function inputChangeCheckbox(inputEvent) {
    document.querySelector(`[id="encrypt-checkbox-${extractFieldName(inputEvent.target)}"]`).disabled = inputEvent.target.value === '';
  }
  /**
   * Enable submit if and only if all proc text fields are empty or disabled.
   * @param procFields
   * @param procFieldElements
   */
  function switchSubmitOnInput(procFields, procFieldElements) {
    // Process when there is a submit element specified in drupalSettings.
    if (drupalSettings.proc && drupalSettings.proc.submit_element_id) {
      procFields.forEach((fieldName, index) => {
        procFieldElements[index].setAttribute(
          'data-proc-text-siblings',
          procFields.join(),
        );
        procFieldElements[index].addEventListener('input', inputChangeSubmit);
      });
    }
  }
  /**
   * Set data-proc-text-siblings attributes.
   * @param procFields
   * @param procFieldElements
   * @param encryptCheckboxes
   */
  function setTextSiblingsAttribute(
    procFields,
    procFieldElements,
    encryptCheckboxes,
  ) {
    const procFieldsString = procFields.join();
    procFields.forEach((fieldName, index) => {
      procFieldElements[index].setAttribute(
        'data-proc-text-siblings',
        procFieldsString,
      );
      encryptCheckboxes[index].setAttribute(
        'data-proc-text-siblings',
        procFieldsString,
      );
    });
  }
  /**
   * Proc text field event change handler.
   * @param inputEvent
   */
  function inputChangeSubmit(inputEvent) {
    switchSubmit(
      getSubmitElements(),
      getSiblings(inputEvent).every(sibling => {
        const siblingField = document.querySelector(`[name^="${sibling}"]`);
        return siblingField.value === '' || siblingField.style.display === 'none';
      })
    );
  }
  /**
   * Checkbox change event handler for changing submit.
   * @param checkBoxEvent
   */
  function checkboxChangeSubmit(checkBoxEvent) {
    switchSubmit(
      getSubmitElements(),
      getSiblings(checkBoxEvent).every(sibling => {
        const siblingCheckbox = document.querySelector(
          `[id="encrypt-checkbox-${sibling}"]`,
        );
        return siblingCheckbox.checked || siblingCheckbox.disabled;
      })
    );
  }
  /**
   * Enable submit if and only if all checkboxes are checked.
   * @param encryptCheckboxes
   */
  function switchSubmitOnCheckbox(encryptCheckboxes) {
    // Process when there is a submit element specified in drupalSettings.
    if (drupalSettings.proc && drupalSettings.proc.submit_element_id) {
      encryptCheckboxes.forEach((encryptCheckbox) => {
        encryptCheckbox.addEventListener('input', checkboxChangeSubmit);
      });
    }
  }

  /**
   * Switch submit button.
   * @param submitElements
   * @param enable
   */
  function switchSubmit(submitElements, enable) {
    submitElements.forEach(submitElement => {
      // Switch disabled attribute only if necessary and when the submit element exists:
      if (submitElement && submitElement.disabled !== !enable) {
        submitElement.disabled = !enable;
      }
    });
  }
  /**
   * Get submit elements.
   * @return submitElements
   */
  function getSubmitElements() {
    return drupalSettings.proc.submit_element_id
      .replace(/ /g, '')
      .split(',')
      .map(submitId => document.getElementById(submitId));
  }
  /**
   * Get siblings.
   * @param event
   * @return siblings
   */
  function getSiblings(event) {
    return event.target
      .getAttribute('data-proc-text-siblings')
      .split(',');
  }
  Drupal.behaviors.ProcBehavior = ProcBehavior;
})(jQuery, Drupal, drupalSettings, once);
