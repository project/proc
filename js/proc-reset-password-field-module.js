/**
 * @file
 * Reset password field.
 */

/**
 * Reset password field.
 *
 * @param $
 * @param messages
 * @param actionLink
 */
export function resetPasswordField($, messages, actionLink) {
  console.info('Resetting password field.');
  messages.clear();
  $('#edit-password').val('');
  if (!actionLink.hasClass('active')) {
    actionLink
      .removeClass('active')
      .removeAttr('download')
      .removeAttr('href');
  }
}
