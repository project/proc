/**
 * @file
 * Process decryption of cipher text.
 */

import { processCache } from './proc-process-cache-module.js';
import { decryptPrivateKey } from './proc-decrypt-privkey-module.js';
import { procDecrypt } from './proc-decrypt-module.js';
import { handleDecryptionResult } from './proc-handle-decryption-result-module.js';

/**
 * Process decryption.
 *
 * @param $
 * @param procURLs
 * @param openpgp
 * @param privateKey
 * @param passphrase
 * @param temporaryDownloadLink
 * @param opLink
 * @returns {Promise<void>}
 */
export async function processDecryption($, procURLs, openpgp, privateKey, passphrase, temporaryDownloadLink, opLink) {
  await Promise.all(
    drupalSettings.proc.proc_ids.map(async (cipherId, cipherIndex) => {
      let cachedCiphers = await processCache(procURLs, cipherIndex);
      const cipherText = cachedCiphers
        ? (await cachedCiphers.json()).pubkey[0].armored
        : '';
      const decryptedPrivateKey = await decryptPrivateKey($, openpgp, privateKey, passphrase);
      const message = await openpgp.readMessage({
        armoredMessage: cipherText,
      });
      let procFormat = 'binary';
      if (drupalSettings.proc.proc_sources_input_modes[cipherIndex]) {
        procFormat = drupalSettings.proc.proc_sources_input_modes[cipherIndex];
      }
      if (decryptedPrivateKey) {
        console.info('Decrypted private key');
        await handleDecryptionResult(
          $,
          await procDecrypt(
            openpgp,
            decryptedPrivateKey,
            message,
            procFormat,
            $
          ),
          cipherId,
          temporaryDownloadLink,
          cipherIndex,
          opLink
        );
      }
    }),
  );
}
