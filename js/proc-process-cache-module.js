/**
 * @file
 * Process cache.
 */

/**
 * Process cache.
 *
 * @param {string[]} procURLs
 *   Process URLs.
 * @param {number} cipherIndex
 *   Cipher index.
 *
 * @returns {Promise<Response>}
 */
export async function processCache(procURLs, cipherIndex) {
  const cacheDetails = {
    cache: 'caches' in self ? caches.open('proc') : null,
    response: null,
    cipher: null,
    cachedCiphers: null,
  };
  if (cacheDetails.cache) {
    cacheDetails.cachedCiphers = await cacheDetails.cache.then(async function (cache) {
      cacheDetails.response = await cache.match(procURLs[cipherIndex]);
      if (!cacheDetails.response) {
        console.info('Adding cipher to cache');
        const cipherResponse = await fetch(procURLs[cipherIndex]);
        cacheDetails.cipher = cipherResponse.clone();
        await cache.put(procURLs[cipherIndex], cipherResponse);
        return cacheDetails.cipher;
      }
      console.info('Reusing cipher from cache');
      return cacheDetails.response;
    });
  }
  return cacheDetails.cachedCiphers;
}
