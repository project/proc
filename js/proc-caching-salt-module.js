/**
 * Get the password session caching salt.
 *
 * @returns {string}
 */
export function getPassSessionCachingSalt() {
  const _navigator = {};
  const deprecatedItems = [
    'webkitTemporaryStorage',
    'webkitPersistentStorage',
  ];
  for (const i in navigator) {
    if (!(deprecatedItems.indexOf(i) > -1)) {
      _navigator[i] = navigator[i];
    }
  }
  return JSON.stringify(_navigator);
}
