/**
 * @file
 * Handle decryption result.
 */

import { openFileAction } from './proc-open-file-action-module.js';
import { procReadKey } from './proc-pubkey-module.js';
import { procEncrypt } from './proc-encrypt-module.js';

/**
 * Handle decryption result.
 *
 * @param $
 * @param decrypted
 * @param cipherId
 * @param temporaryDownloadLink
 * @param cipherIndex
 * @param opLink
 * @returns {Promise<void>}
 */
export async function handleDecryptionResult($, decrypted, cipherId, temporaryDownloadLink, cipherIndex, opLink) {
  if (opLink === undefined) {
    opLink = $('#decryption-link');
  }
  if (decrypted === undefined) {
    const msg = Drupal.t(
      'Unable to decrypt the content. Make sure you have entered the right passphrase.',
    );
    // Log warning on console.
    console.warn(msg);
    const message = new Drupal.Message();
    $('form[id^="proc"]').prepend(message.messageWrapper);
    message.add(msg, {type: 'error'});
  }
  if (decrypted) {
    if (opLink[0].id !== 'update-link') {
      // Simple encryption:
      if (
        $('textarea').length !== 0 &&
        (typeof decrypted.data === 'string' ||
          decrypted.data instanceof String)
      ) {
        $(`#edit-${cipherId}`)[0].innerText = decrypted.data;
      } else {
        await openFileAction(
          $,
          decrypted,
          temporaryDownloadLink,
          cipherIndex,
          opLink,
        );
      }
    }
    else {
      // Re-encryption:
      const plaintext = decrypted.data;
      let reader = new FileReader();
      reader.readAsArrayBuffer(new Blob([plaintext], {type: 'application/octet-binary',endings: 'native'}));
      reader.onloadend = async function (evt) {
        if (evt.target.readyState === FileReader.DONE) {
          let array = new Uint8Array(evt.target.result);
          const startSeconds    = new Date().getTime() / 1000;
          let filteredData = Object.fromEntries(
            Object.entries(
              JSON.parse(drupalSettings.proc.proc_recipients_pubkeys_changed))
                .filter(([key]) => JSON.parse(drupalSettings.proc.proc_selected_update_procs_recipients)[cipherId].includes(key)
            )
          );
          drupalSettings.proc.proc_recipients_pubkeys_changed = JSON.stringify(filteredData);
          console.info(`Re-encrypting proc ID ${cipherId} for user(s) with ID(s): ${Object.keys(filteredData).toString()}`);
          const encrypted = await procEncrypt({'binary' : array}, await procReadKey($, drupalSettings, openpgp), openpgp);
          let endSeconds = new Date().getTime() / 1000;
          if (encrypted) {
            $(`input[name=cipher_text_${cipherId}]`)[0].value = encrypted;
            $(`input[name=cipher_text_${cipherId}]`).trigger('change');
            $(`input[name=browser_fingerprint_${cipherId}]`)[0].value = `${navigator.userAgent}, (${screen.width} x ${screen.height})`;
            $(`input[name=generation_timestamp_${cipherId}]`)[0].value = startSeconds;
            $(`input[name=generation_timespan_${cipherId}]`)[0].value = endSeconds - startSeconds;
            $(`input[name=signed_${cipherId}]`)[0].value = 0;
          }
        }
      };
    }
  }
}
