/**
 * @file
 * Openpgp decryption handler.
 */

/**
 * Decrypt the message using the decrypted private key.
 *
 * @param {object} openpgp
 *   Openpgp library.
 * @param {object} decryptedPrivateKey
 *   Decrypted private key.
 * @param {string} message
 *   Message to decrypt.
 * @param {string} procFormat
 *   Format of the message.
 * @param {object} $
 *   jQuery.
 *
 * @returns {promise}
 */
export async function procDecrypt(openpgp, decryptedPrivateKey, message, procFormat, $) {
  return await openpgp
    .decrypt({
      decryptionKeys: decryptedPrivateKey,
      message,
      format: procFormat,
    })
    .catch(function (err) {
      const message = new Drupal.Message();
      $('form[id^="proc"]').prepend(message.messageWrapper);
      message.add(`${Drupal.t(err)}`, {type: 'error'});
    });
}
