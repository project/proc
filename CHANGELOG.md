# Changelog

All notable changes to this project will be documented in this file.

## [10.1.81] - 11 Sep 2024

- Added rendering of SVG image files at armoured field formatter.

## [10.1.80] - 9 Sep 2024

- Fixed bug in absence of CC recipients by direct fetcher.
- Added decryption of multiple files in a single click.

## [10.1.79] - 23 Aug 2024

- Fixed bug on key creation.
- Added fetcher endpoint to cipher metadata.
- Required permission for encryption by anonymous user.

## [10.1.78] - 6 Aug 2024

- Fixed bug on trying to access a proc with id ZERO.

## [10.1.77] - 2 Aug 2024

- Disabled autocomplete on proc file fields for non-admin users by default.

## [10.1.76] - 1 Aug 2024

- Fixed bug on submission of existing content by users not included in the set
of recipients.

## [10.1.75] - 31 Jul 2024

- Removed the decrypt button from disabled text fields.

## [10.1.74] - 22 Jul 2024

- Removed reset button if field is disabled.

## [10.1.73] - 19 Jul 2024

- Added configuration for the label of reset button.

## [10.1.72] - 18 Jul 2024 at 20:45 CEST

- Removed yet another type declaration for backwards compatibility.

## [10.1.71] - 18 Jul 2024 at 20:39 CEST

- Removed yet another type declaration for backwards compatibility.

## [10.1.70] - 17 Jul 2024

- Fixed bug on the selection of recipients under permissive policy.

## [10.1.69] - 11 Jul 2024

- Added multimedia support on armoured formatter.
- Added original file name to download of unrecognized mime type files.
- Added temporary download link to unrecognized mime types in decryption of lists.

## [10.1.68] - 5 Jul 2024

- Added option for a reset button in single-valued proc file fields.

## [10.1.67] - 4 Jul 2024 at 13:55 CEST

- Rolled back from FormElementBase to FormElement.

## [10.1.66] - 4 Jul 2024 at 12:39 CEST

- Replaced ProcAjaxModalMsgController by directly appending a message.
- Added fall back to default label of encrypt button.
- Added autocomplete attribute on password field.
- Increased default value for maximum size of encryption.

## [10.1.65] - 18 Jun 2024

- Fixed bug on the change of a proc field label.
- Added on-the-fly decryption of default text.

## [10.1.64] - 25 May 2024

- Fixed selection of delta on proc file field widget being shown in a dialog.
